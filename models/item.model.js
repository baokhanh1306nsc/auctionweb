const mongoose = require('mongoose');
const autoIncrement = require('mongoose-sequence')(mongoose);
const itemSchema = new mongoose.Schema({
    itemId: {
        type: Number
    },
    name: {
        type: String,
        required: true
    },
    category: {
        id:{
            type: mongoose.Types.ObjectId,
            ref: 'Category'
        },
        name: String
    },
    priceInit: {
        type: Number,
        required: true
    },
    priceNow: {
        type: Number,
        required: true
    },
    priceInstant: {
        type: Number
    },
    owner: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    bidder: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    startDate: {
        type: Date,
        default: Date.now
    },
    endDate: {
        type: Date,
        required: true
    },
    bidCounts: {
        type: Number,
        default: 0
    },
    detailDescription: {
        type: String,
        required: true
    },
    jump: {
        type: Number,
        required: true
    },
    history: [
        {
            bidder: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User'
            },
            price: Number,
            time: {
                type: Date,
                default: Date.now
            }
        }
    ],
    status: {
        type: Boolean,
        default: true
    },
    images: [String],
    banList: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }
    ],
    fresh: {
        type: Boolean,
        default: true
    },
    autobid: [{
        user: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        },
        priceMax: Number
    }]
});

itemSchema.plugin(autoIncrement, {id: 'item_seq', inc_field: 'itemId'});
itemSchema.index({name: 'text','category.name':'text'});
const Item = mongoose.model('item', itemSchema);

module.exports = Item;
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const sellerSchema = new mongoose.Schema({
    user: {type: Schema.Types.ObjectId, ref: 'User', required: true},
    sellNow: [{type: Schema.Types.ObjectId, ref: 'item'}],
    sellWon: [{
        item: {
            type: Schema.Types.ObjectId,
            ref: 'item'
        },
        comment: {
            type: Boolean,
            default: false
        }
    }],
    pendingBidder: [{
        user: {type: Schema.Types.ObjectId, ref: 'User'},
        item: {type: Schema.Types.ObjectId, ref: 'item'},
        status: Boolean
    }]
});

const Seller = mongoose.model('Seller', sellerSchema);
module.exports = Seller;

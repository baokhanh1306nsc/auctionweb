const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;

const adminSchema = new mongoose.Schema({
    user: { type: ObjectId, ref: 'User', required: true },
    pendingBidder: [{ type: ObjectId, ref: 'User'}]
});

const Admin = mongoose.model('Admin', adminSchema);
module.exports = Admin;
const mongoose = require('mongoose');
const autoIncrement = require('mongoose-sequence')(mongoose);
const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;

const categorySchema = new mongoose.Schema({
   name: {
       type: String,
       required: true
   },
    catId: Number,
    items: [{
       type: ObjectId,
        ref: 'item'
    }]
});

categorySchema.plugin(autoIncrement, {id: 'cat_seq', inc_field: 'catId'});
const Category = mongoose.model('Category', categorySchema);

module.exports = Category;
const mongoose = require('mongoose');
const autoIncrement = require('mongoose-sequence')(mongoose);
const Schema = mongoose.Schema;
const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        trim: true
    },
    address: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        trim: true
    },
    password: {
        type: String,
        required: true
    },
    active : {
        type: Boolean,
        default: false
    },
    verifyCode: {
        type: Number,
    },
    type: {
        type: Number,
        default: 0
    },
    userId: {
        type: Number
    },
    watchList: [{type: Schema.Types.ObjectId, ref: 'item'}],
    reviews: {
        upVote: {
            type: Number,
            default: 0
        },
        downVote: {
            type: Number,
            default: 0
        },
        comments: [
            {
                user: {
                    type: Schema.Types.ObjectId,
                    ref: 'User'
                },
                item: {
                    type: Schema.Types.ObjectId,
                    ref: 'item'
                },
                comment: String,
                vote: Boolean
            }
        ]
    },
    bidNow: [{
        item: {
            type: Schema.Types.ObjectId,
            ref: 'item'
        },
        canBid: {
            type: Boolean,
            default: false
        }
    }
    ],
    bidWon: [
        {
            item: {
                type: Schema.Types.ObjectId,
                ref: 'item'
            },
            comment: {
                type: Boolean,
                default: false
            }
        }
    ],
});

userSchema.plugin(autoIncrement, {id: 'user_seq', inc_field: 'userId'});

const User = mongoose.model('User', userSchema);

module.exports = User;
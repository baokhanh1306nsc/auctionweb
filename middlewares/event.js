const EventEmitter = require('events').EventEmitter;
const event = new EventEmitter();
const Item = require('../models/item.model');
const User = require('../models/user.model');
const Seller = require('../models/seller.model');

event.on('autobid', async (price,itemId) => {
   const item = await Item.findById(itemId);
   let autobid = item.autobid;
   price = price + item.jump;
   autobid = autobid.filter(bid => bid.priceMax >= price);
    if (autobid.length === 0){
        item.autobid = autobid;
        await item.save();
        return;
    }
   if (autobid.length === 1){
       item.priceNow = price;
       item.history.push({ price, bidder: autobid[0].user, time:new Date() });
       item.bidCounts++;
       item.bidder = autobid[0].user;
       if (item.priceNow === item.priceInstant){
           item.status = false;
           const user = await User.findOne({_id: item.bidder});
           for (let i = user.bidNow.length - 1; i >= 0; i--) {
               if (user.bidNow[i].item.equals(item._id)) {
                   user.bidNow.splice(i, 1);
                   break;
               }
           }
           user.bidWon.push({item: item._id, comment: false});
           await user.save();
           await Seller.findOneAndUpdate({user: item.owner}, {
               $addToSet: {sellWon: {item: item._id, comment: false}},
               $pull: {sellNow: item._id}
           })
       }
       item.autobid = autobid;
       await item.save();
       return;
   }
   for (let i = 0; i < autobid.length - 1; i++){
       for (let j = i + 1; j < autobid.length; j++){
           if (autobid[i].priceMax >= autobid[j].priceMax){
               let auto = autobid[i];
               autobid[i] = autobid[j];
               autobid[j] = auto;
           }
       }
   }
   if (autobid[autobid.length - 2].priceMax === autobid[autobid.length - 1].priceMax){
       price = autobid[autobid.length - 2].priceMax;
   }
   else {
       price = autobid[autobid.length - 2].priceMax + item.jump;
       item.history.push({price:autobid[autobid.length - 2].priceMax, bidder:autobid[autobid.length - 2].user, time: new Date() })
   }
    item.priceNow = price;
    item.history.push({ price, bidder: autobid[autobid.length - 1].user, time:new Date() });
    item.bidCounts++;
    item.bidder = autobid[autobid.length - 1].user;
    let maxBid = autobid[autobid.length - 1];
    item.autobid = [];
    item.autobid.push(maxBid);
    await item.save();
});


module.exports = event;
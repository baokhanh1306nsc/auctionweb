module.exports = {

    ensureAuthenticated: function (req, res, next) {
        if (req.isAuthenticated()){
            res.locals.user = req.user;
            return next();
        }
        req.flash('error_msg', 'Please log in');
        res.redirect('/user/login');
    },

    ensureSeller: (req,res,next) => {
        if (req.user.type === 1){
            return next();
        }
        req.flash('error_msg', 'Please log in with another seller account');
        res.redirect('/user/login');
    },

    ensureAdmin: (req, res, next) => {
        if (req.user.type === 2){
            return next();
        }
        req.flash('error_msg', 'Please log in with another admin account');
        res.redirect('/user/login');
    },

    redirectAdmin: (req, res, next) => {
        if (req.user) {
            if (req.user.type === 2) {
                return res.redirect('/admin/category');
            }
        }
        next();
    }
};
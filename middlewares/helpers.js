const Item = require('../models/item.model');
const User = require('../models/user.model');
const Seller = require('../models/seller.model');

exports.updateStatus = async () => {
    const date = new Date();
    const items = await Item.find({status: true});
    try {
        for (item of items) {
            if (item.endDate.getTime() - date.getTime() <= 0) {
                item.status = false;
                await item.save();
                await User.findOneAndUpdate({_id: item.bidder}, {
                    $addToSet: {bidWon: {item: item._id, comment: false}}
                });
                await Seller.findOneAndUpdate({_id: item.owner},{
                    $addToSet: {sellWon: {item: item._id, comment: false}}
                });
            }
        }
    } catch (e) {
        throw (e);
    }
};

exports.time = function (current, previous) {
    var msPerMinute = 60 * 1000;
    var msPerHour = msPerMinute * 60;
    var msPerDay = msPerHour * 24;
    var msPerMonth = msPerDay * 30;

    const limit = msPerDay * 3;
    var elapsed = current - previous;
    if (elapsed < 0) {
        return 'Đã kết thúc.';
    }
    if (elapsed <= limit) {
        if (elapsed < msPerMinute) {
            return 'Còn ' + Math.round(elapsed / 1000) + ' giây nữa';
        } else if (elapsed < msPerHour) {
            return 'Còn ' + Math.round(elapsed / msPerMinute) + ' phút nữa';
        } else if (elapsed < msPerDay) {
            return 'Còn ' + Math.round(elapsed / msPerHour) + ' giờ nữa';
        } else if (elapsed < msPerMonth) {
            return 'Còn ' + Math.round(elapsed / msPerDay) + ' ngày nữa';
        }
    } else {
        return null;
    }
};

exports.pagination = (pageTotal, page) => {
    const pages = [];
    for (let i = 0; i < pageTotal; i++) {
        pages[i] = {
            value: i + 1,
            active: (i + 1) === page
        };
    }
    const navs = {};
    if (page > 1) {
        navs.prev = page - 1;
    }
    if (page < pageTotal) {
        navs.next = page + 1;
    }
    return {pages, navs};
};

exports.checkBid = (upVote, downVote, canBid) => {
    if (upVote === 0 && downVote === 0) {
        if (canBid) {
            return 2;
        }
        return 1;
    }
    let sum = upVote + downVote;
    let vote = upVote / sum;
    if (vote >= 0.8) {
        return 2;
    }
    return 0;
};

exports.itemDate = (items) => {
    for (item of items) {
        item.startTime = item.startDate.toLocaleString().trim();
        const date = new Date();
        const relativeTime = this.time(item.endDate, date);
        if (relativeTime != null) {
            item.endTime = relativeTime;
        } else {
            item.endTime = item.endDate.toLocaleString().trim();
        }
    }
};

exports.setWatched = (user, items) => {
    if (user) {
        for (item of items) {
            item.isWatched = user.watchList.includes(item._id);
        }
    }
};
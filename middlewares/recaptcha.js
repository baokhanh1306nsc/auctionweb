const fetch = require('node-fetch');

exports.verifyCaptcha = async (req, res, next) => {
    const captcha = req.body['g-recaptcha-response'];
    if(captcha === undefined || captcha === '' || captcha === null) {
        req.flash('error_msg','Please select captcha');
        res.redirect('/user/login');
        return;
    }
    const secretKey = process.env.CAPTCHA_SECRET;
    const verificationUrl = `https://www.google.com/recaptcha/api/siteverify?secret=${secretKey}&response=${captcha}&remoteip=${req.connection.remoteAddress}`;
    const response = await fetch(verificationUrl);
    const body = await response.json();
    console.log(body);
    if (body.success !== undefined && !body.success){
        req.flash('error_msg','Failed captcha verified');
        res.redirect('/user/login');
    }
    else{
        next();
    }
};
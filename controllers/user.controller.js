const User = require('../models/user.model');
const helpers = require('../middlewares/helpers');
const bcrypt = require('bcryptjs');
const mongoose = require('mongoose');
const nodemailer = require('nodemailer');
const Admin = require('../models/admin.model');
const ObjectId = mongoose.Types.ObjectId;

exports.save = async(req, res, next) => {
    const { name, address, email, password, password2 } = req.body;

    let errors = [];

    if (password.length < 6) {
        errors.push({ msg: 'Password must be at least 6 characters!' });
    }

    if (password2 != password) {
        errors.push({ msg: 'Password do not match.' });
    }

    if (errors.length > 0) {
        res.render('register', {
            title: 'Register',
            layout: 'user.hbs',
            errors,
            name,
            address,
            email
        });
    }
    try {
        const user = await User.findOne({ email: email });
        if (user) {
            errors.push({ msg: 'Email already exists' });
            res.render('register', {
                title: 'Register',
                layout: 'user.hbs',
                errors,
                name,
                address
            });
        } else {
            const rand = Math.floor(Math.random() * 100 + 54);
            const newUser = new User({
                name,
                address,
                email,
                password,
                verifyCode: rand
            });
            const salt = await bcrypt.genSalt(10);
            const hash = await bcrypt.hash(newUser.password, salt);
            newUser.password = hash;
            await newUser.save();
            const link = `localhost:3000/user/verify?id=${newUser.userId}&verifyCode=${rand}`;
            const smtpTransport = nodemailer.createTransport({
                service: 'Gmail',
                auth: {
                    user: process.env.EMAIL_USER,
                    pass: process.env.EMAIL_PASS
                }
            });
            const mailOptions = {
                to: `${newUser.email}`,
                subject: 'Confirm your email',
                html: `Hello,<br> Please Follow the link to verify your email.<br>${link}`
            };
            smtpTransport.sendMail(mailOptions, (error, response) => {
                if (error) next(error);
                else {
                    req.flash('success_msg', 'Please confirm your email and log in');
                    res.redirect('/user/login');
                }
            });
        }
    } catch (e) {
        next(e);
    }
};

exports.verify = async(req, res, next) => {
    try {
        const id = req.query.id;
        const code = parseInt(req.query.verifyCode);
        const user = await User.findOne({ userId: id });
        if (user) {
            if (code === user.verifyCode && user.active === false) {
                user.active = true;
                user.verifyCode = undefined;
                await user.save();
                req.flash('success_msg', 'Now you can log in with your account');
                return res.redirect('/user/login');
            }
        }
        req.flash('error_msg', 'Something wrong with your verify link');
        res.redirect('/user/login');
    } catch (e) {
        next(e);
    }
};

exports.dashboard = (req, res, next) => {
    res.render('dashboard', {
        user: req.user,
        profile: true
    });
};

exports.request = async(req, res, next) => {
    const user = req.user;
    try {
        res.render('dashboard', {
            user,
            request: true
        });
    } catch (e) {
        next(e);
    }
};

exports.postRequest = async(req, res, next) => {
    const user = req.user;
    try {
        const admin = await Admin.find({ pendingBidder: user.id });
        if (admin === null || admin === [] || admin.length === 0) {
            await Admin.updateMany({}, {
                $addToSet: { pendingBidder: user.id }
            });
            req.flash('success_msg', 'Yêu cầu của bạn đã được gởi cho ban quản trị.');
            res.redirect('/user/request');
            return;
        }
        req.flash('error_msg', 'Vui lòng đợi ban quản trị duyệt yêu cầu của bạn.');
        res.redirect('/user/request');
    } catch (e) {
        next(e);
    }
};

exports.change = async(req, res, next) => {
    const { name, email, password, password2 } = req.body;
    const user = req.user;

    let errors = [];

    if (password.length < 6) {
        errors.push({ msg: 'Password must be at least 6 characters!' });
    }

    if (password2 != password) {
        errors.push({ msg: 'Password do not match.' });
    }

    bcrypt.compare(password, user.password, (error, isMatch) => {
        if (error) next(error);
        if (!isMatch) {
            errors.push({ msg: 'Password is incorrect' });
        }
    });

    if (errors.length > 0) {
        res.render('dashboard', {
            user,
            errors,
            profile: true
        });
    } else {
        await User.findOneAndUpdate({ userId: user.userId }, { name: name }, { new: true });
        req.flash('success_msg', 'Update success');
        res.redirect('/user/dashboard');
    }
};

exports.getChangePassword = async(req, res, next) => {
    res.render('dashboard', {
        user: req.user,
        changePassword: true
    });
};

exports.postChangePassword = async(req, res, next) => {
    try {
        const { password, newpass, newpass1 } = req.body;
        const user = req.user;

        let errors = [];

        if (password.length < 6) {
            errors.push({ msg: 'Password must be at least 6 characters!' });
        }

        if (newpass1 != newpass) {
            errors.push({ msg: 'Password do not match.' });
        }

        if (errors.length > 0) {
            res.render('dashboard', {
                user,
                errors,
                changePassword: true
            });
        } else {
            bcrypt.compare(password, user.password, async(error, isMatch) => {
                if (error) next(error);
                if (isMatch) {
                    user.password = newpass;
                    const salt = await bcrypt.genSalt(10);
                    const hash = await bcrypt.hash(user.password, salt);
                    user.password = hash;
                    await user.save();
                    req.flash('success_msg', 'Your password has been successfully changed');
                    res.redirect('/user/changePassword');
                } else {
                    req.flash('error_msg', 'Password incorrect');
                    res.redirect('/user/changePassword');
                }
            });
        }
    } catch (e) {
        next(e);
    }
};


exports.reviews = async(req, res, next) => {
    const page = parseInt(req.query.page) || 1;
    try {
        const user = await User.findOne({ userId: req.user.userId })
            .populate('reviews.comments.user')
            .populate('reviews.comments.item');
        const total = user.reviews.comments.length;
        const pageSize = 5;
        const pageTotal = Math.ceil(total / pageSize);
        const comments = user.reviews.comments.slice((page - 1) * pageSize, pageSize * page);
        const { pages, navs } = helpers.pagination(pageTotal, page);
        res.render('dashboard', {
            reviews: true,
            comments,
            pages,
            navs,
            user
        });
    } catch (e) {
        next(e);
    }
};

exports.watchList = async(req, res, next) => {
    const page = parseInt(req.query.page) || 1;
    try {
        const user = await User.findOne({ userId: req.user.userId })
            .populate('watchList');
        const total = user.watchList.length;
        const pageSize = 5;
        const pageTotal = Math.ceil(total / pageSize);
        const { pages, navs } = helpers.pagination(pageTotal, page);
        const items = user.watchList.slice((page - 1) * pageSize, pageSize * page);
        helpers.itemDate(items);
        res.render('dashboard', {
            watch: true,
            items,
            pages,
            navs
        });
    } catch (e) {
        next(e);
    }
};

//TODO: implement now bidding list
exports.now = async(req, res, next) => {
    const page = parseInt(req.query.page) || 1;
    try {
        const user = await User.findOne({ userId: req.user.userId })
            .populate({
                path: 'bidNow.item',
                match: { status: true },
            });
        const total = user.bidNow.length;
        const pageSize = 5;
        const pageTotal = Math.ceil(total / pageSize);
        const { pages, navs } = helpers.pagination(pageTotal, page);
        const items = user.bidNow.slice((page - 1) * pageSize, pageSize * page);
        for (let i of items) {
            i.wining = i.item.bidder.equals(user.id);
            i.item.startTime = i.item.startDate.toLocaleString().trim();
            const date = new Date();
            const relativeTime = helpers.time(i.item.endDate, date);
            if (relativeTime != null) {
                i.item.endTime = relativeTime;
            } else {
                i.item.endTime = i.item.endDate.toLocaleString().trim();
            }
        }
        res.render('dashboard', {
            now: true,
            items,
            pages,
            navs
        });
    } catch (e) {
        next(e);
    }
};

exports.history = async(req, res, next) => {
    const page = parseInt(req.query.page) || 1;
    try {
        const user = await User.findOne({ userId: req.user.userId })
            .populate({
                path: 'bidWon.item',
                populate: {
                    path: 'owner',
                    model: 'User'
                }
            });
        const total = user.bidWon.length;
        const pageSize = 5;
        const pageTotal = Math.ceil(total / pageSize);
        const { pages, navs } = helpers.pagination(pageTotal, page);
        const items = user.bidWon.slice((page - 1) * pageSize, pageSize * page);
        res.render('dashboard', {
            history: true,
            items,
            pages,
            navs
        });
    } catch (e) {
        next(e);
    }
};

exports.comment = async(req, res, next) => {
    const { itemId, ownerId, bidderId, feel, comment } = req.body;
    const backURL = req.header('Referer') || '/';
    try {
        let vote = feel === 'like';
        const user = mongoose.Types.ObjectId(bidderId);
        const item = mongoose.Types.ObjectId(itemId);
        const owner = await User.findOne({ _id: ownerId });
        if (vote) {
            owner.reviews.upVote++;
        } else {
            owner.reviews.downVote++;
        }
        owner.reviews.comments.push({
            user,
            item,
            comment,
            vote
        });
        await owner.save();
        const bidder = await User.findOne({ _id: bidderId });
        for (bidWon of bidder.bidWon) {
            if (bidWon.item.equals(itemId)) {
                bidWon.comment = true;
                break;
            }
        }
        await bidder.save();
        res.redirect(backURL);
    } catch (e) {
        next(e);
    }
};

exports.reviewById = async(req, res, next) => {
    const id = req.params.id;
    const page = parseInt(req.query.page) || 1;
    try {
        const user = await User.findById(ObjectId(id))
            .populate('reviews.comments.user')
            .populate('reviews.comments.item');
        const review = user.reviews;
        let comments = review.comments;
        const total = comments.length;
        const pageSize = 5;
        const pageTotal = Math.ceil(total / pageSize);
        comments = comments.slice((page - 1) * pageSize, pageSize * page);
        const { pages, navs } = helpers.pagination(pageTotal, page);
        res.render('userReview',{
            comments,
            pages,
            navs,
            info: user,
            user
        });
    } catch (e) {
        next(e);
    }
};
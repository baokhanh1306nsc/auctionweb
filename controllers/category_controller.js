const Item = require('../models/item.model');
const Category = require('../models/category.model');
const helpers = require('../middlewares/helpers');

exports.filteredByName = async (req, res, next) => {
    try {
        await helpers.updateStatus();
        const name = req.params.name;
        const pageSize = 6;
        const page = parseInt(req.query.page) || 1;
        const sortOption = req.query.sortBy;
        const all = await Item.find({'category.name': name});
        const total = all.length;
        const pageTotal = Math.ceil(total / pageSize);
        const offset = (page - 1) * pageSize;
        let items = [];
        if (sortOption === 'default') {
            items = await Item.find({ 'category.name': name })
                .skip(offset)
                .limit(pageSize)
                .populate('owner')
                .populate('bidder');
        } else {
            if (sortOption === 'time') {
                items = await Item.find({ 'category.name': name })
                    .sort({ endDate: 'desc' })
                    .skip(offset)
                    .limit(pageSize)
                    .populate('owner')
                    .populate('bidder');
            } else {
                items = await Item.find({ 'category.name': name })
                    .sort({ priceNow: 'asc' })
                    .skip(offset)
                    .limit(pageSize)
                    .populate('owner')
                    .populate('bidder');
            }
        }
        const pages = [];
        for (let i = 0; i < pageTotal; i++) {
            pages[i] = {
                value: i + 1,
                active: (i + 1) === page,
                name,
                sortOption
            };
        }
        const navs = {};
        if (page > 1) {
            navs.prev = page - 1;
        }
        if (page < pageTotal) {
            navs.next = page + 1;
        }
        helpers.itemDate(items);
        helpers.setWatched(req.user, items);

        res.render('category', {
            user: req.user,
            items,
            pages,
            navs,
            name,
            sortOption
        });
    } catch (e) {
        next(e);
    }
};

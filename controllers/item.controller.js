const Item = require('../models/item.model');
const helpers = require('../middlewares/helpers');
const User = require('../models/user.model');
const Category = require('../models/category.model');
const Seller = require('../models/seller.model');
const MaskData = require('maskdata');
const nodemailer = require('nodemailer');
const event = require('../middlewares/event');

const maskNameOptions = {
    maskWith: "*",
    unmaskedStartCharacters : 0,
    unmaskedEndCharacters : 4
};


const mostItems = async(item, lim) => {
    try {
        const items = await Item.find({ status: 'true' }).sort(item).limit(lim)
            .populate('owner')
            .populate('bidder');

        for (item of items) {
            if (item.bidder != null)
                item.bidder.maskedName = MaskData.maskPhone(item.bidder.name, maskNameOptions);
        }

        return items;
    } catch (e) {
        throw e;
    }
};

const totalItems = async() => {
    try {
        const total = await Item.countDocuments({});
        return total;
    } catch (e) {
        throw e;
    }
};

exports.all = async(req, res, next) => {
    try {
        const pageSize = 6;
        const page = parseInt(req.query.page) || 1;
        const total = await totalItems();
        const pageTotal = Math.ceil(total / pageSize);
        const offset = (page - 1) * pageSize;

        const all = await Item.find({})
            .skip(offset)
            .limit(pageSize)
            .populate('owner')
            .populate('bidder');


        for (item of all) {
            if (item.bidder != null)
                item.bidder.maskedName = MaskData.maskPhone(item.bidder.name, maskNameOptions);
        }

        const { pages, navs } = helpers.pagination(pageTotal, page);
        helpers.itemDate(all);
        helpers.setWatched(req.user, all);
        res.render('itemAll', {
            user: req.user,
            items: all,
            pages,
            navs
        });
    } catch (e) {
        next(e);
    }
};

exports.detail = async(req, res, next) => {
    try {
        const id = parseInt(req.query.id) || 1;
        const item = await Item.findOne({ itemId: id })
            .populate('owner')
            .populate('bidder')
            .populate('history.bidder');
        let isOwner = false;
        if (item.bidder != null)
            item.bidder.maskedName = MaskData.maskPhone(item.bidder.name, maskNameOptions);
        if (req.isAuthenticated()) {
            isOwner = item.owner.id === req.user.id;
        }
        item.hintPrice = item.priceNow + item.jump;
        item.startTime = item.startDate.toLocaleString().trim();
        const date = new Date();
        const relativeTime = helpers.time(item.endDate, date);
        if (relativeTime != null) {
            item.endTime = relativeTime;
        } else {
            item.endTime = item.endDate.toLocaleString().trim();
        }
        const totalItems = await Item.find({});
        const randomOffset = Math.floor(Math.random() * (totalItems / 5));
        const randomItems = await Item.find({ category: item.category, itemId: { $ne: item.itemId } })
            .skip(randomOffset)
            .limit(5);
        for (history of item.history) {
            history.maskedName = MaskData.maskPhone(history.bidder.name, maskNameOptions);
            history.bidTime = history.time.toLocaleString().trim();
        }
        let imgs = [];
        for (image of item.images) {
            imgs.push({
                value: image,
                isActive: false
            })
        }
        imgs[0].isActive = true;
        res.render('itemDetail', {
            user: req.user,
            item,
            randomItems,
            imgs: imgs,
            isOwner
        });
    } catch (e) {
        next(e);
    }
};


exports.index = async(req, res, next) => {
    const categories = await Category.find({});
    try {
        const mostEnd = await mostItems({ endDate: 'asc' }, 5);
        const mostBid = await mostItems({ bidCounts: -1 }, 5);
        const mostPrice = await mostItems({ priceNow: -1 }, 5);

        helpers.itemDate(mostEnd);
        helpers.itemDate(mostBid);
        helpers.itemDate(mostPrice);

        helpers.setWatched(req.user, mostEnd);
        helpers.setWatched(req.user, mostBid);
        helpers.setWatched(req.user, mostPrice);


        res.render('index', {
            title: "Auction",
            user: req.user,
            categories,
            items_1: mostEnd,
            items_2: mostBid,
            items_3: mostPrice
        })
    } catch (e) {
        next(e);
    }
};

exports.find = async(req, res, next) => {
    try {
        const query = req.query.query;
        const pageSize = 6;
        const page = parseInt(req.query.page) || 1;
        const sortOption = req.query.sortBy;
        const all = await Item.find({ $text: { $search: query } });
        const total = all.length;
        const pageTotal = Math.ceil(total / pageSize);
        const offset = (page - 1) * pageSize;
        let items = [];
        if (sortOption === 'default') {
            items = await Item.find({ $text: { $search: query } })
                .skip(offset)
                .limit(pageSize)
                .populate('owner')
                .populate('bidder');
            if (items.bidder != null)
                items.bidder.maskedName = MaskData.maskPhone(items.bidder.name, maskNameOptions);
        } else {
            if (sortOption === 'time') {
                items = await Item.find({ $text: { $search: query } })
                    .sort({ endDate: 'desc' })
                    .skip(offset)
                    .limit(pageSize)
                    .populate('owner')
                    .populate('bidder');
                if (items.bidder != null)
                    items.bidder.maskedName = MaskData.maskPhone(items.bidder.name, maskNameOptions);
            } else {
                items = await Item.find({ $text: { $search: query } })
                    .sort({ priceNow: 'asc' })
                    .skip(offset)
                    .limit(pageSize)
                    .populate('owner')
                    .populate('bidder');
                if (items.bidder != null)
                    items.bidder.maskedName = MaskData.maskPhone(items.bidder.name, maskNameOptions);
            }
        }
        const pages = [];
        for (let i = 0; i < pageTotal; i++) {
            pages[i] = {
                value: i + 1,
                active: (i + 1) === page,
                query,
                sortOption
            };
        }
        const navs = {};
        if (page > 1) {
            navs.prev = page - 1;
        }
        if (page < pageTotal) {
            navs.next = page + 1;
        }
        helpers.itemDate(items);
        helpers.setWatched(req.user, items);

        res.render('itemFind', {
            user: req.user,
            items,
            pages,
            navs,
            query,
            sortOption
        });
    } catch (e) {
        next(e);
    }
};

exports.favorite = async(req, res, next) => {
    const id = req.params.id || 0;
    const backURL = req.header('Referer') || '/';
    if (id === 0) {
        res.redirect(backURL);
    }
    try {
        const item = await Item.findOne({ itemId: id, status: true });
        if (item) {
            await User.findOneAndUpdate({ userId: req.user.userId }, {
                $addToSet: { watchList: item._id }
            });
            res.redirect(backURL);
        } else {
            res.redirect(backURL);
        }
    } catch (e) {
        next(e);
    }
};

exports.bid = async(req, res, next) => {
    const user = req.user;
    const itemId = parseInt(req.query.item) || 0;
    if (itemId === 0) {
        res.redirect(`/`);
        return;
    }
    try {
        const item = await Item.findOne({ itemId: itemId });
        if (!item.status) {
            req.flash('error_msg', 'Đã kết thúc');
            res.redirect(`/detail?id=${itemId}`);
            return;
        }
        if (item.owner.equals(user.id)) {
            req.flash('error_msg', 'Không thể tự đấu giá sản phẩm của mình!');
            res.redirect(`/detail?id=${itemId}`);
            return;
        }
        if (item.banList.includes(user.id)) {
            req.flash('error_msg', 'Bạn không đủ điều kiện đấu giá');
            res.redirect(`/detail?id=${itemId}`);
            return;
        }
        let permission;
        for (bidNow of user.bidNow) {
            if (bidNow.item.equals(item._id)) {
                permission = bidNow.canBid;
            }
        }
        let canBid = helpers.checkBid(user.reviews.upVote, user.reviews.downVote, permission);
        if (!canBid) {
            req.flash('error_msg', 'Bạn không đủ điều kiện đấu giá');
            res.redirect(`/detail?id=${itemId}`);
            return;
        }
        const price = req.body.price;
        const bidderId = user.userId;
        const time = new Date();
        const id = user._id;
        if (canBid === 1) {
            let isIncluded = false;
            for (bidNow of user.bidNow) {
                if (bidNow.item.equals(item._id)) {
                    isIncluded = true;
                    break;
                }
            }
            if (!isIncluded) {
                await User.findOneAndUpdate({ userId: bidderId }, {
                    $addToSet: { bidNow: { item: item._id, canBid: 0 } }
                });
                await Seller.findOneAndUpdate({ user: item.owner }, {
                    $addToSet: { pendingBidder: { user: user._id, item: item._id } }
                });
            }
            req.flash('error_msg', 'Vui lòng đợi người bán duyệt!');
            res.redirect(`/detail?id=${itemId}`);
        } else {
            let win = false;
            item.history.push({ price, bidder: id, time });
            item.bidCounts++;
            item.priceNow = price;
            if (item.priceNow === item.priceInstant){
                item.status = false;
                win = true;
            }
            item.bidder = id;
            await item.save();
            if (item.status) {
                let isIncluded = false;
                for (bidNow of user.bidNow) {
                    if (bidNow.item.equals(item._id)) {
                        isIncluded = true;
                        break;
                    }
                }
                if (!isIncluded) {
                    await User.findOneAndUpdate({userId: bidderId}, {
                        $addToSet: {bidNow: {item: item._id, canBid: 1}}
                    });
                }
                event.emit('autobid', item.priceNow, item._id);
            }
            if (win){
                const user = await User.findOne({_id: item.bidder});
                    for (let i = user.bidNow.length - 1; i >= 0; i--) {
                        if (user.bidNow[i].item.equals(item._id)) {
                            user.bidNow.splice(i, 1);
                            break;
                        }
                    }
                    user.bidWon.push({item: item._id, comment: false});
                    await user.save();
                    await Seller.findOneAndUpdate({user: item.owner}, {
                    $addToSet: {sellWon: {item: item._id, comment: false}},
                    $pull: {sellNow: item._id}
                })
            }
            const smtpTransport = nodemailer.createTransport({
                service: 'Gmail',
                auth: {
                    user: process.env.EMAIL_USER,
                    pass: process.env.EMAIL_PASS
                }
            });
            const mailOptions = {
                to: `${user.email}`,
                subject: 'Bạn đã đấu giá thành công',
                html: `<h3>Chúc mừng</h3><br>Bạn vừa đấu giá thành công sản phẩm ${item.name}.`
            };
            await smtpTransport.sendMail(mailOptions);
            req.flash('success_msg', 'Đấu giá thành công');
            res.redirect(`/detail?id=${itemId}`);
        }
    } catch (e) {
        next(e);
    }
};

exports.update = async (req, res, next) => {
  const id = req.params.id;
  const { info } = req.body;
  const backUrl = req.header('Referer') || '/';
  try{
      const item = await Item.findOne({_id: id});
      item.detailDescription += info;
      await item.save();
      res.redirect(backUrl);
  }catch (e) {
      next(e);
  }
};

exports.autobid = async (req ,res, next) => {
  const user = req.user;
  const itemId = parseInt(req.query.item) || 0;
  const {priceMax} = req.body;
  const backUrl = req.header('Referer') || '/';
  try{
      const item = await Item.findOne({itemId: itemId});
      item.autobid.push({
          user: user._id,
          priceMax
      });
      await item.save();
      event.emit('autobid', item.priceNow, item._id);
      req.flash('success_msg','Đấu giá tự động thành công');
      res.redirect(backUrl);
  }catch (e) {
      next(e);
  }
};
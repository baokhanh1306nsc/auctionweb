const User = require('../models/user.model');
const Item = require('../models/item.model');
const Admin = require('../models/admin.model');
const Category = require('../models/category.model');
const Seller = require('../models/seller.model');
const mongoose = require('mongoose');


exports.category = async(req, res) => {
    const categories = await Category.find({});
    res.render('adminCategory', {
        layout: 'admin',
        title: 'Category',
        categories,
        user: req.user,
        index: true
    });
};

exports.item = async(req, res) => {
    const items = await Item.find({});
    const categories = await Category.find({});
    res.render('adminItem', {
        layout: 'admin',
        title: 'Item',
        items,
        user: req.user,
        categories
    });
};


exports.itemByCategory = async(req, res, next) => {
    try {
        const name = req.params.name;
        const categories = await Category.find({});
        const items = await Item.find({ 'category.name': name });
        res.render('adminItem', {
            layout: 'admin',
            title: 'Item',
            items,
            user: req.user,
            categories
        });
    } catch (e) {
        next(e);
    }
};

exports.user = async(req, res) => {
    try {
        const users = await User.find({});
        const categories = await Category.find({});
        for (user of users) {
            if (user.type === 0) {
                user.role = 'Bidder';
            } else if (user.type === 1) {
                user.role = 'Seller';
            } else {
                user.role = 'Admin';
            }
        }
        res.render('adminUser', {
            layout: 'admin',
            title: 'User',
            users,
            user: req.user,
            categories,
            index: true
        });
    } catch (e) {
        next(e);
    }

};

exports.detailUser = async(req, res, next) => {
    try {
        const id = req.params.id;
        const user = await User.findOne({ _id: id })
            .populate('reviews.comments.user')
            .populate('reviews.comments.item');
        const review = user.reviews;
        let comments = review.comments;

        const categories = await Category.find({});
        if (user.type === 0) {
            user.role = 'Bidder';
        } else if (user.type === 1) {
            user.role = 'Seller';
        } else {
            user.role = 'Admin';
        }

        res.render('detailUser', {
            layout: 'admin',
            title: 'User',
            user,
            comments,
            categories,
            index: true
        });
    } catch (e) {
        next(e);
    }

};

exports.getEditCategory = async(req, res, next) => {
    const id = req.query.id;
    try {
        const categories = await Category.find({});
        for (let category of categories) {
            category.isEdit = false;
            if (category.id === id) {
                category.isEdit = true;
            }
        }
        res.render('adminCategory', {
            layout: 'admin',
            title: 'Category',
            categories,
            user: req.user,
            edit: true
        })
    } catch (e) {
        next(e);
    }
};


exports.getPendingBidder = async(req, res, next) => {
    try {
        const categories = await Category.find({});
        const admins = await Admin.findOne({ _id: '5e0866ff7cd3de07e011daec' })
            .populate('pendingBidder');
        res.render('adminPendingBidder', {
            layout: 'admin',
            title: 'Pending Bidder',
            admins,
            categories,
            user: req.user,
            index: true
        });
    } catch (e) {
        next(e);
    }

};



exports.postEditCategory = async(req, res, next) => {
    const { name, id } = req.body;
    try {
        await Category.findOneAndUpdate({ _id: id }, {
            name: name
        });
        await Item.updateMany({ 'category.id': id }, {
            'category.name': name
        });
        res.redirect('/admin/category');
    } catch (e) {
        next(e);
    }
};

exports.UpgradeToSeller = async(req, res, next) => {
    const id = req.params.id;
    try {
        await User.findOneAndUpdate({ _id: id }, {
            type: 1
        });
        const seller = new Seller({
            user: mongoose.Types.ObjectId(id)
        });
        await seller.save();
        await Admin.findOneAndUpdate({ _id: '5e0866ff7cd3de07e011daec' }, {
            $pull: { pendingBidder: mongoose.Types.ObjectId(id) }
        });
        req.flash('success_msg', 'Upgrade user successfully');
        res.redirect('/admin/user/pendingBidder');
    } catch (e) {
        next(e);
    }
};

exports.refuseUpgrade = async(req, res, next) => {
    const id = req.params.id;
    try {
        await Admin.findOneAndUpdate({ _id: '5e0866ff7cd3de07e011daec' }, {
            $pull: { pendingBidder: mongoose.Types.ObjectId(id) }
        });
        res.redirect('/admin/user/pendingBidder');
    } catch (e) {
        next(e);
    }
};


exports.deleteCategory = async(req, res, next) => {
    const id = req.query.id;
    try {
        const category = await Category.findOne({ _id: id });
        if (category.items.length > 0) {
            req.flash('error_msg', 'Can not delete category having items');
            res.redirect('/admin/category');
        } else {
            await Category.deleteOne({ _id: id });
            res.redirect('/admin/category');
        }
    } catch (e) {
        next(e);
    }
};

exports.deleteItem = async(req, res, next) => {
    const id = req.query.id;
    try {
        await Item.deleteOne({ _id: id });
        res.redirect('/admin/item');
    } catch (e) {
        next(e);
    }
};

exports.deleteUser = async(req, res, next) => {
    const id = req.query.id;
    try {
        await User.deleteOne({ _id: id });
        res.redirect('/admin/user');
    } catch (e) {
        next(e);
    }
};

exports.getAddCategory = async(req, res, next) => {
    try {
        const categories = await Category.find({});
        res.render('adminCategory', {
            layout: 'admin',
            title: 'Category',
            categories,
            user: req.user,
            add: true
        });
    } catch (e) {
        next(e);
    }
};

exports.postAddCategory = async(req, res, next) => {
    const { name } = req.body;
    try {
        const category = new Category({
            name
        });
        await category.save();
        res.redirect('/admin/category');
    } catch (e) {
        next(e);
    }
};
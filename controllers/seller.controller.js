const User = require('../models/user.model');
const Item = require('../models/item.model');
const Seller = require('../models/seller.model');
const Category = require('../models/category.model');
const helpers = require('../middlewares/helpers');
const nodemailer = require('nodemailer');
const mongoose = require('mongoose');
const schedule = require('node-schedule');

exports.sell = async (req, res, next) => {
    try {
        const categories = await Category.find({});
        res.render('dashboard', {
            isSelling: true,
            categories
        })
    } catch (e) {
        next(e);
    }
};

exports.now = async (req, res, next) => {
    const page = parseInt(req.query.page) || 1;
    try {
        const seller = await Seller.findOne({user: req.user.id})
            .populate('sellNow');
        let items = seller.sellNow;
        const total = items.length;
        const pageSize = 5;
        const pageTotal = Math.ceil(total / pageSize);
        const {pages, navs} = helpers.pagination(pageTotal, page);
        items = items.slice((page - 1) * pageSize, pageSize * page);
        for (item of items) {
            const date = new Date();
            const relativeTime = helpers.time(item.endDate, date);
            if (relativeTime != null) {
                item.endTime = relativeTime;
            } else {
                item.endTime = item.endDate.toLocaleString().trim();
            }
        }
        res.render('dashboard', {
            sellNow: true,
            items,
            pages,
            navs
        })
    } catch (e) {
        next(e);
    }
};

exports.postItem = async (req, res, next) => {
    const {name, initPrice, jump, instantPrice, info, endDate, category} = req.body;
    const backURL = req.header('Referer') || '/';
    const images = req.files;
    if (images.length < 3){
        req.flash('error_msg', 'Cần phải đăng ít nhất 3 ảnh');
        res.redirect(backURL);
        return;
    }
    try {
        const cat = await Category.findOne({name: category});
        const item = new Item({
            name,
            category: {
                id: cat._id,
                name: cat.name
            },
            priceNow: initPrice,
            priceInit: initPrice,
            priceInstant: instantPrice,
            owner: req.user.id,
            endDate,
            detailDescription: info,
            jump,
            images: []
        });
        for (let image of images) {
            item.images.push(image.path);
        }
        await item.save();
        cat.items.push(item);
        await cat.save();
        await Seller.findOneAndUpdate({user: req.user.id}, {
            $addToSet: {sellNow: item.id}
        });
        const now = new Date();
        const jobFresh = schedule.scheduleJob(now.getTime() + (30*1000), async () => {
            await Item.findOneAndUpdate({_id: item.id}, {
                fresh: false
            });
        });
        const j = schedule.scheduleJob(endDate, async () => {
            const newItem = await Item.findOne({_id: item.id});
            if (!newItem.status) {
                newItem.status = false;
                await newItem.save();
                const user = await User.findOne({_id: newItem.bidder});
                if (user !== null) {
                    for (let i = user.bidNow.length - 1; i >= 0; i--) {
                        if (user.bidNow[i].item.equals(item._id)) {
                            user.bidNow.splice(i, 1);
                            break;
                        }
                    }
                    user.bidWon.push({item: newItem.id, comment: false});
                    await user.save();
                }
                await Seller.findOneAndUpdate({user: newItem.owner}, {
                    $addToSet: {sellWon: {item: newItem.id, comment: false}},
                    $pull: {sellNow: item.id}
                });
            }
            });
        req.flash('success_msg', 'Update success');
        res.redirect(backURL);
    } catch (e) {
        next(e);
    }
};

exports.ignore = async (req, res, next) => {
    const userId = req.query.user || '';
    const itemId = req.query.item || '';
    const backUrl = req.header('Referer') || '/';
    if (userId === '' || itemId === '') {
        res.redirect(backUrl);
        return;
    }
    try {
        const uid = mongoose.Types.ObjectId(userId);
        const iid = mongoose.Types.ObjectId(itemId);
        const bidder = await User.findOne({_id: uid});
        const item = await Item.findOne({_id: iid});
        item.history = item.history.filter(history => !history.bidder.equals(uid));
        const historyLength = item.history.length;
        if (historyLength === 0) {
            item.priceNow = item.priceInit;
            item.bidder = null;
            item.bidCounts = 0;
        } else {
            item.priceNow = item.history[historyLength - 1].price;
            item.bidder = item.history[historyLength - 1].bidder;
        }
        item.banList.push(uid);
        for (bidNow of bidder.bidNow) {
            if (bidNow.item.equals(iid)) {
                bidNow.canBid = false;
                break;
            }
        }
        await bidder.save();
        await item.save();
        const smtpTransport = nodemailer.createTransport({
            service: 'Gmail',
            auth: {
                user: process.env.EMAIL_USER,
                pass: process.env.EMAIL_PASS
            }
        });
        const mailOptions = {
            to: `${bidder.email}`,
            subject: 'Bạn đã bị từ chối đấu giá',
            html: `<h3>Lưu ý</h3><br>Bạn vừa bị từ chối đấu giá cho sản phẩm ${item.name}.`
        };
        await smtpTransport.sendMail(mailOptions);
        res.redirect(backUrl);
    } catch (e) {
        next(e);
    }
};

exports.sold = async (req, res, next) => {
    const page = parseInt(req.query.page) || 1;
    try {
        const seller = await Seller.findOne({user: req.user.id})
            .populate({
                path:'sellWon.item',
                populate: {
                    path: 'bidder',
                    model: 'User'
                }
            });
        let items = seller.sellWon;
        const total = items.length;
        const pageSize = 5;
        const pageTotal = Math.ceil(total / pageSize);
        const {pages, navs} = helpers.pagination(pageTotal, page);
        items = items.slice((page - 1) * pageSize, pageSize * page);
        res.render('dashboard', {
            sold: true,
            items,
            pages,
            navs
        })
    } catch (e) {
        next(e);
    }
};

exports.comment = async (req, res, next) => {
    const {itemId, ownerId, bidderId, feel, comment} = req.body;
    const backURL = req.header('Referer') || '/';
    try {
        let vote = feel === 'like';
        const user = mongoose.Types.ObjectId(ownerId);
        const item = mongoose.Types.ObjectId(itemId);
        const bidder = await User.findOne({_id: bidderId});
        if (vote) {
            bidder.reviews.upVote++;
        } else {
            bidder.reviews.downVote++;
        }
        bidder.reviews.comments.push({
            user,
            item,
            comment,
            vote
        });
        await bidder.save();
        const owner = await Seller.findOne({user: ownerId});
        for (sellWon of owner.sellWon) {
            if (sellWon.item.equals(itemId)) {
                sellWon.comment = true;
                break;
            }
        }
        await owner.save();
        res.redirect(backURL);
    } catch (e) {
        next(e);
    }
};

exports.pending = async (req, res, next) => {
    const page = parseInt(req.query.page) || 1;
  try{
      const seller = await Seller.findOne({user: req.user.id})
          .populate('pendingBidder.user')
          .populate('pendingBidder.item');
      const items = seller.pendingBidder;
      const total = items.length;
      const pageSize = 10;
      const pageTotal = Math.ceil(total / pageSize);
      const {pages, navs} = helpers.pagination(pageTotal, page);
      const pendingItems = items.slice((page - 1) * pageSize, pageSize * page);
      res.render('dashboard', {
          pending: true,
          pendingItems,
          pages,
          navs
      })
  }  catch (e) {
      next(e);
  }
};

exports.upgrade = async (req, res, next) => {
    const id = req.query.id;
    const uid = req.query.uid;
  try{
      const bidder = await User.findById(uid);
      const seller = await Seller.findOne({user:req.user.id});
      const items = seller.pendingBidder;
      for (let bid of bidder.bidNow){
          if (bid.item.equals(id)){
              bid.canBid = true;
              break;
          }
      }
      for (let i = seller.pendingBidder.length - 1; i >= 0; i--){
          if (seller.pendingBidder[i].user.equals(uid) && seller.pendingBidder[i].item.equals(id)){
              seller.pendingBidder.splice(i,1);
              break;
          }
      }
      await seller.save();
      await bidder.save();
      res.redirect('/user/pending');
  }  catch (e) {
      next(e);
  }
};

exports.refuse = async (req, res, next) => {
    try{
        const id = req.query.id;
        const uid = req.query.uid;
        const bidder = await User.findById(uid);
        const seller = await Seller.findOne({user:req.user.id});
        const item = await Item.findById(id);
        for (let i = bidder.bidNow.length - 1; i >= 0; i--){
            if (bidder.bidNow[i].item.equals(id)){
                bidder.bidNow.splice(i,1);
                break;
            }
        }
        for (let i = seller.pendingBidder.length - 1; i >= 0; i--){
            if (seller.pendingBidder[i].user.equals(uid) && seller.pendingBidder[i].item.equals(id)){
                seller.pendingBidder.splice(i,1);
                break;
            }
        }
        item.banList.push(mongoose.Types.ObjectId(uid));
        await seller.save();
        await bidder.save();
        await item.save();
        res.redirect('/user/pending');
    }  catch (e) {
        next(e);
    }
};

const express = require('express');
const router = express.Router();
const item_controller = require('../controllers/item.controller');
const {ensureAuthenticated, ensureSeller, redirectAdmin} = require('../middlewares/auth');


router.get('/watch/:id', ensureAuthenticated, redirectAdmin,item_controller.favorite);

router.post('/bid', ensureAuthenticated, redirectAdmin,item_controller.bid);

router.post('/update/:id', ensureAuthenticated, redirectAdmin,ensureSeller, item_controller.update);

router.post('/autobid', ensureAuthenticated, redirectAdmin, item_controller.autobid);
module.exports = router;
const express = require('express');
const router = express.Router();
const user_controller = require('../controllers/user.controller');
const seller_controller = require('../controllers/seller.controller');
const passport = require('passport');
const recaptcha = require('../middlewares/recaptcha');
const { ensureAuthenticated, ensureSeller, redirectAdmin } = require('../middlewares/auth');

const multer = require('multer');

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './uploads');
    },
    filename: function(req, file, cb) {
        cb(null, new Date().getTime() + '_' + file.originalname);
    }
});

const upload = multer({ storage: storage });


router.get('/login', (req, res) => {
    res.render('login', {
        title: 'Login',
        layout: 'user.hbs',
        site: process.env.CAPTCHA_SITE
    });
});

router.post('/login', recaptcha.verifyCaptcha, (req, res, next) => {
    passport.authenticate('local', {
        successRedirect: '/',
        failureRedirect: '/user/login',
        failureFlash: true
    })(req, res, next);
});

router.get('/register', (req, res) => {
    res.render('register', {
        title: 'Register',
        layout: 'user.hbs'
    })
});

router.post('/register', user_controller.save);

router.get('/verify', user_controller.verify);


router.get('/logout', (req, res) => {
    req.logout();
    res.redirect('/');
});


router.get('/dashboard', ensureAuthenticated, redirectAdmin, user_controller.dashboard);

router.post('/dashboard', ensureAuthenticated, redirectAdmin, user_controller.change);

router.get('/changePassword', ensureAuthenticated, redirectAdmin, user_controller.getChangePassword);

router.post('/changePassword', ensureAuthenticated, redirectAdmin, user_controller.postChangePassword);

router.get('/reviews', ensureAuthenticated, redirectAdmin, user_controller.reviews);

router.get('/watch', ensureAuthenticated, redirectAdmin, user_controller.watchList);

router.get('/now', ensureAuthenticated, redirectAdmin, user_controller.now);

router.get('/history', ensureAuthenticated, redirectAdmin, user_controller.history);

router.post('/comment', ensureAuthenticated, redirectAdmin, user_controller.comment);

router.get('/request', ensureAuthenticated, redirectAdmin, user_controller.request);

router.post('/request', ensureAuthenticated, redirectAdmin, user_controller.postRequest);

router.get('/sell', ensureAuthenticated, redirectAdmin, ensureSeller, seller_controller.sell);

router.post('/sell', ensureAuthenticated, redirectAdmin, ensureSeller, upload.array('images', 12), seller_controller.postItem);

router.get('/ignore', ensureAuthenticated, redirectAdmin, ensureSeller, seller_controller.ignore);

router.get('/sellNow', ensureAuthenticated, redirectAdmin, ensureSeller, seller_controller.now);

router.get('/sold', ensureAuthenticated, redirectAdmin, ensureSeller, seller_controller.sold);

router.post('/commentBidder', ensureAuthenticated, redirectAdmin, ensureSeller, seller_controller.comment);

router.get('/pending', ensureAuthenticated, redirectAdmin, ensureSeller, seller_controller.pending);

router.get('/accept', ensureAuthenticated, redirectAdmin, ensureSeller, seller_controller.upgrade);

router.get('/refuse', ensureAuthenticated, redirectAdmin, ensureSeller, seller_controller.refuse);

router.get('/:id/review', redirectAdmin, user_controller.reviewById);

module.exports = router;
const express = require('express');
const router = express.Router();
const Item = require('../models/item.model');
const Category = require('../models/category.model');
const Admin = require('../models/admin.model');
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;
router.post('/item', async (req, res) => {
    try {
        await Item.updateMany({status: false},{
            status: true
        });
        res.send('ok');
    } catch (e) {
        res.status(500).send(e);
    }
});

router.get('/item', async (req,res) => {
    try {
        await Item.updateMany({'category.name':'Shoes'},{
            priceInit: 2000,
            priceInstant: 5000,
            jump: 200,
            priceNow: 2000
        });
        res.send('ok');
    } catch (e) {
        res.status(500).send(e);
    }
});
router.post('/category', async (req, res) => {
    const {name} = req.body;
    try {
        const items = await Item.find({type: 'laptop'}, '_id');
        const cat = new Category({
            name,
            items
        });
        await cat.save();
        res.send('ok');
    } catch (e) {
        res.status(500).send(e);
    }
});

router.post('/admin', async (req, res) => {
    const {id} = req.body;
    try{
        const admin = new Admin({
            user: ObjectId(id)
        });
        await admin.save();
        res.send('ok');
    }catch (e) {
        res.status(500).end(e);
    }
});

router.post('/seller', (req, res) => {

});

router.get('/demo', async (req, res, next) => {
    try {
        res.render('demo');
    } catch (e) {
        next(e);
    }
});

router.post('/demo',  async (req,res) => {
    const images = req.files;
    const id = req.body.id;
    const item = await Item.findOne({itemId: id});
    

    for (let image of images){
        item.images.push(image.path);
    }
    await item.save();
    res.render('demo');
});

module.exports = router;
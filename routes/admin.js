const express = require('express');
const router = express.Router();
const admin_controller = require('../controllers/admin.controller');
const { ensureAuthenticated, ensureAdmin } = require('../middlewares/auth');

router.get('/category', ensureAuthenticated, ensureAdmin, admin_controller.category);
router.get('/category/edit', ensureAuthenticated, ensureAdmin, admin_controller.getEditCategory);
router.post('/category/edit', ensureAuthenticated, ensureAdmin, admin_controller.postEditCategory);
router.get('/category/delete', ensureAuthenticated, ensureAdmin, admin_controller.deleteCategory);
router.get('/category/add', ensureAuthenticated, ensureAdmin, admin_controller.getAddCategory);
router.post('/category/add', ensureAuthenticated, ensureAdmin, admin_controller.postAddCategory);

router.get('/item', ensureAuthenticated, ensureAdmin, admin_controller.item);
router.get('/item/delete', ensureAuthenticated, ensureAdmin, admin_controller.deleteItem);
router.get('/item/:name', ensureAuthenticated, ensureAdmin, admin_controller.itemByCategory);


router.get('/users', ensureAuthenticated, ensureAdmin, admin_controller.user);
router.get('/users/:id', ensureAuthenticated, ensureAdmin, admin_controller.detailUser);
router.get('/user/pendingBidder', ensureAuthenticated, ensureAdmin, admin_controller.getPendingBidder);
router.get('/user/upgrade/:id', ensureAuthenticated, ensureAdmin, admin_controller.UpgradeToSeller);
router.get('/user/refuse/:id', ensureAuthenticated, ensureAdmin, admin_controller.refuseUpgrade);
router.get('/user/delete', ensureAuthenticated, ensureAdmin, admin_controller.deleteUser);



module.exports = router;
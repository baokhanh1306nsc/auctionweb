const express = require('express');
const router = express.Router();
const category_controller = require('../controllers/category_controller');
const {redirectAdmin} = require('../middlewares/auth');


/* GET home page. */
router.get('/:name',redirectAdmin,category_controller.filteredByName);

module.exports = router;
var express = require('express');
var router = express.Router();
const item_controller = require('../controllers/item.controller');
const {redirectAdmin} = require('../middlewares/auth');

/* GET home page. */
router.get('/',redirectAdmin,item_controller.index);

router.get('/all',redirectAdmin,item_controller.all);

router.get('/detail',redirectAdmin, item_controller.detail);

router.get('/search', redirectAdmin,item_controller.find);

module.exports = router;
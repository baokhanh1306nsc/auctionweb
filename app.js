const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mongoose = require('mongoose');
const exphbs = require('express-handlebars');
const flash = require('connect-flash');
const session = require('express-session');
const passport = require('passport');
const exphbsSection = require('express-handlebars-sections');
const MaskData = require('maskdata');


require('./config/passport')(passport);
require('dotenv').config();
//routes
const indexRouter = require('./routes/index');
const userRouter = require('./routes/users');
const itemRouter = require('./routes/item');
const adminRouter = require('./routes/admin');
const seedRouter = require('./routes/seed');
const categoryRouter = require('./routes/category');

//view engine
const hbs = exphbs.create({
    defaultLayout: 'main',
    extname: 'hbs',
    helpers: require('./config/handlebars-helpers')
});

exphbsSection(hbs);

//connect to db
mongoose.connect(process.env.DB_PASS, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => console.log('Mongodb connected'))
    .catch(err => console.log(err));
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);

const app = express();


// Express session
app.use(
    session({
        secret: 'secret',
        resave: true,
        saveUninitialized: true
    })
);
app.engine('hbs', hbs.engine);
app.set('view engine', 'hbs');
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/uploads', express.static('uploads'));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());

// Global variables
app.use(function(req, res, next) {
    res.locals.success_msg = req.flash('success_msg');
    res.locals.error_msg = req.flash('error_msg');
    res.locals.error = req.flash('error');
    next();
});


app.use('/', indexRouter);
app.use('/user', userRouter);
app.use('/item', itemRouter);
app.use('/admin', adminRouter);
app.use('/seed', seedRouter);
app.use('/category', categoryRouter);

require('./middlewares/errors')(app);

module.exports = app;